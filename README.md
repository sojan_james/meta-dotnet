# README #

Meta layer to build the Microsoft .NET coreclr and corefx.

### What is this repository for? ###

* Build the .NET coreclr and corefx. See https://github.com/dotnet
* Version

### How do I get set up? ###

* First follow instructions at https://github.com/kraj/meta-clang 
* Checkout this project
* Edit conf/bblayers.conf to add meta-dotnet to layers. Enable clang as the default toolchain as in KRaj's instructions.


* Dependencies
  meta-clang from https://github.com/kraj/meta-clang