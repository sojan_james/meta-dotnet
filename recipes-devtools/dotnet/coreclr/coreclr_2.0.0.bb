# Microsoft dotnet CLR
# This contains the .NET Core runtime, called CoreCLR, and the base library, 
# called mscorlib. It includes the garbage collector, JIT compiler, 
# base .NET data types and many low-level classes.
# Warning: This does not work yet!

# Depends on meta-clang
# Currently using meta-clang        = "master:625f4efb211f33e69144cd3e1e65ca581151d201"
 
DESCRIPTION = "Microsoft dotnet coreclr"
SECTION = "devtools"
DEPENDS = "clang-native lldb libunwind gettext icu util-linux python-native ca-certificates-native lttng-ust"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.TXT;md5=9fc642ff452b28d62ab19b7eea50dfb9"

#FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

SRCREV = "fd7b8ca0faaa21fde687db55e124bf0fd683aec1"
SRC_URI = "git://github.com/dotnet/coreclr.git;branch=master \
	   file://0001_Disable_cmake_checks.patch \
           file://0003_toolchain_setup.patch \
           file://0004_support_i586_pal.patch \
           file://0005_Fix_assignments.patch \
           file://0006_fix_hex_in_asm.patch \
           file://0007_x64_cross.patch \
           file://0008_casts.patch \
           "

DOTNET_ARCH_i586 = "x86"
DOTNET_ARCH_x86-64 = "x64"
DOTNET_ARCH_armv6 = "arm"
DOTNET_ARCH_arm64 = "arm64"


S = "${WORKDIR}/git"
inherit cmake

TARGET_CXXFLAGS += "-std=c++11"

do_configure() {
	export ROOT_DIR="${STAGING_DIR_TARGET}"
	export WORKDIR="${WORKDIR}"
        cd ${S}
	echo ./build.sh ${DOTNET_ARCH} debug configureonly cross  crosscomponent clang4.0
	./build.sh ${DOTNET_ARCH} debug configureonly cross crosscomponent clang4.0
}

do_compile() {
	export ROOT_DIR="${STAGING_DIR_TARGET}"
	export WORKDIR="${WORKDIR}"
        cd ${S}
	echo ./build.sh ${DOTNET_ARCH} debug skipconfigure cross clang4.0
	./build.sh ${DOTNET_ARCH} debug skipconfigure cross crosscomponent clang4.0
}



